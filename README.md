# Deploy Omnibus to AWS

## Purpose

Intended for support use this script provides a fast, repeatable, reliable and customizable method 
of deploying GitLab into Amazon EC2 by executing the official install steps in a bash script.

It will:

- Download and install GitLab to Ubuntu 
- Make GitLab available at `https://gitlab-<ec2-public-ip>.nip.io`

## Getting started

UI deployment:

1. From the EC2 manager click "Launch instances" and enter your preferred values, ensure you select an Ubuntu OS image. 
1. Cut and paste the script into "Advanced Settings" -> "User Data"
1. Edit the VERSION variable at the top of the script and set to preferred version e.g VERSION="15.11.13"
1. Click launch instance

Or use [Terraform](terraform/).

## Notes

- **Should not be used for production deployments**
- This will deploy a "bare-bones" instance with no data.  If you want a fully featured deployment use a [support-resources sandbox](https://about.gitlab.com/handbook/infrastructure-standards/realms/sandbox/) instead.
- Does not depend on any external DNS configuration in AWS
- Deployment will take a little while to complete after the instance starts
- Deployment status can be watched with `sudo tail -f /var/log/cloud-init-output.log`

