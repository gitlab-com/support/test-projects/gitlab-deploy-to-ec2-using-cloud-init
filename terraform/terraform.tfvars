# gitlab_version must be in format <major.minor.patch>
gitlab_version_to_install = "15.6.1"
# ubuntu 20.04 LTS
ami = "ami-0138de5989ddf0636"
# instance_type = "t3.medium"
# instance_type = "t3.large"
instance_type = "t3.xlarge"
