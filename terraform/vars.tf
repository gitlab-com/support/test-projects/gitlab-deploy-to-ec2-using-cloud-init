variable "ami" {
  type = string
}

variable "instance_type" {
  type = string
}

variable "gitlab_initial_root_password" {
  type = string
}

variable "gitlab_version_to_install" {
  type        = string
  description = "GitLab version to install"
}
