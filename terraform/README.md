## Terraform provisioning for AWS

#### Installation

1. Install `terraform` following [HashiCorp Terraform documentation](https://learn.hashicorp.com/tutorials/terraform/install-cli)
1. Install `aws cli` using [AWS documentation](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
1. Create authentication credentials (Access Key and Secret access key) in AWS following [this documentation](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html#cli-configure-quickstart-creds)
1. Configure `aws` cli following [the documentation here](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html) 

#### How to

- Terraform will be using local backend to store the state files.
- `your_public_ip` is used to limit security rules (ssh and http/https) to your ip only
- your pub ssh key from `~/.ssh/id_rsa.pub` will be installed to ec2 instance
- port 22 is limited to your public ip

1. Initialise terraform with `terraform init`
1. (Optional) Adjust the required variables in `terraform.tfvars` file
1. Check the future setup with `terraform plan`
1. Create the infrastructure with `terraform apply` - terraform will ask for initial root password

    Terraform will provision version 15.2.0 by default. If you want a different version, update `terraform.tfvars` variable `gitlab_version_to_install` to your needs
    Terraform will prompt for an initial root password.  If you want a default password, update `terraform.tfvars` variable `gitlab_initial_root_password` to your needs

1. Wait for the instance to be provisioned

Do not forget to destroy the environment when you finished with the testing

1. Destroy with `terraform destroy`, provide the required input variables, confirm
