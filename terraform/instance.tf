data "external" "your_public_ip" {
  program = ["bash", "./get_my_ip.sh"]
}

data "aws_vpc" "default" {
  default = true
}

locals {
  template_file_runner_config = templatefile("${path.module}/../cloud-init-deploy.sh", {
    gitlab_version_to_install    = var.gitlab_version_to_install
    gitlab_initial_root_password = var.gitlab_initial_root_password
  })
}

resource "random_string" "prefix" {
  length  = 4
  special = false
}

resource "aws_key_pair" "ssh-key" {
  key_name   = "ssh-key-${random_string.prefix.result}"
  public_key = file("~/.ssh/id_rsa.pub")
}

resource "aws_instance" "gitlab_ee" {
  # hardcoded ami for ubuntu 20.04 LTS
  ami           = var.ami
  instance_type = var.instance_type
  #   user_data     = data.template_file.user_script.rendered
  user_data = local.template_file_runner_config
  # user_data_replace_on_change = true
  security_groups = [aws_security_group.sg_gitlab.name]
  key_name        = "ssh-key-${random_string.prefix.result}"

  root_block_device {
    volume_size = 20
  }

  tags = {
    Name = "gitlab-ee-terraform-${random_string.prefix.result}"
  }
}

output "gitlab_instance_details" {
  value       = aws_instance.gitlab_ee.public_ip
  description = "Gitlab instance Public IP"
}

output "gitlab_web_ui" {
  value       = "https://gitlab-${aws_instance.gitlab_ee.public_ip}.nip.io"
  description = "Gitlab instance Public IP"
}

output "ssh" {
  value       = "ssh ubuntu@${aws_instance.gitlab_ee.public_ip}"
  description = "ssh into the instance"
}

output "initial_root_password" {
  value       = var.gitlab_initial_root_password
  description = "Gitlab initial root password"
}

output "gitlab_version" {
  value       = var.gitlab_version_to_install
  description = "Gitlab version installed"
}
