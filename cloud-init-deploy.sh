#!/bin/bash
VERSION="${gitlab_version_to_install}"
PUBLIC_IP="$(TOKEN=`curl -sS -X PUT "http://169.254.169.254/latest/api/token" -H "X-aws-ec2-metadata-token-ttl-seconds: 21600"` && curl -H "X-aws-ec2-metadata-token: $TOKEN" -sS http://169.254.169.254/latest/meta-data/public-ipv4)"
HOSTNAME="gitlab-$PUBLIC_IP.nip.io"
RBFILE="/etc/gitlab/gitlab.rb"

sudo apt-get update
sudo apt-get install -y curl openssh-server ca-certificates tzdata perl
systemctl enable sshd
systemctl start sshd

curl -s https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash

# sed -i "s/\/el\/7/\/amazon\/2/g" /etc/yum.repos.d/gitlab_gitlab*.repo
# yum clean metadata
# yum makecache

EXTERNAL_URL="https://$HOSTNAME" GITLAB_ROOT_PASSWORD="${gitlab_initial_root_password}" apt-get install -y gitlab-ee=$VERSION-ee.0

gitlab-ctl reconfigure
